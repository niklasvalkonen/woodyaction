﻿using UnityEngine;
using System.Collections;

public class BatScript : MonoBehaviour
{

    public static float batSpeed = 0.01f;
    GameObject Selection;

    void Start()
    {
        Selection = this.transform.FindChild("Selection").gameObject;
    }


    void Update()
    {
        


        if (BatAttackController.attacking)
        {

            this.transform.position = Vector3.MoveTowards(
                                        this.transform.position,
                                        GameObject.Find("Peasant 1(Clone)").transform.position,
                                        batSpeed);
        }
    }

    void OnTriggerEnter(Collider other)
    {
        if (other.gameObject == GameObject.Find("Peasant 1(Clone)"))
        {
            Destroy(other.gameObject);
            Application.LoadLevel("DeathScene");

        }
    }
    void OnMouseDown()
    {

        Selection.SetActive(true);
    }
}
