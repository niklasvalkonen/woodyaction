﻿using UnityEngine;
using System.Collections;

public class ClickMove : MonoBehaviour
{

    enum MouseButton { Left = 0, Right = 1, Middle = 2 };
    public Vector3 destination;
    bool goForTarget = false;
    bool isJumping = false;
    Vector3 jumpVector;
    public float jumpForce = 1.0f;
    Vector3 previousPosition;
    float stayedVerticallyInPlace = 0.0f;
    public float timeToStayVerticallyInPlace = 0.25f;

    public bool ableToMove = true;


    // Use this for initialization
    void Awake()
    {
        // pakotetaan objekti säilymään tasojen välillä
        DontDestroyOnLoad(this.gameObject);
    }

    Vector3 GetClosestPointOnFirstRay(Ray r1, Ray r2)
    {

        float v1sqr = Vector3.Dot(r1.direction, r1.direction);
        float v2sqr = Vector3.Dot(r2.direction, r2.direction);
        float v1dotv2 = Vector3.Dot(r1.direction, r2.direction);
        Vector3 s2_s1 = r2.origin - r1.origin;
        float prefix = 1.0f / (v1dotv2 * v1dotv2 - v1sqr * v2sqr);
        float t1 = (-v2sqr * Vector3.Dot(s2_s1, r1.direction)) +
                    (v1dotv2 * Vector3.Dot(s2_s1, r2.direction));
        t1 *= prefix;

        return r1.origin + (r1.direction * t1);
    }


    // Update is called once per frame
    void Update()
    {
        if (ableToMove == true)
        {

            if (Input.GetAxis("Horizontal") > 0.0f)
            {
                animation.CrossFade("run");
            }
            else
            {
                animation.CrossFade("idle");
            }

            // luetaan hiiren vasemman näppäimen vapautus
            if (Input.GetMouseButtonUp((int)MouseButton.Left))
            {
                Ray screenRay = Camera.main.ScreenPointToRay(Input.mousePosition);
                RaycastHit hitInfo;
                bool hits = Physics.Raycast(screenRay, out hitInfo);
                if (hits)
                {
                    Item item = hitInfo.collider.gameObject.GetComponent<Item>();
                    if (item != null)
                    {
                        Debug.Log("Got description: " + item.description);
                    }
                    else
                    {
                        Debug.Log("Hit: " + hitInfo.collider.gameObject.name);
                    }
                }
                // päätelllään mihin kohtaan pelaajan pitää siirtyä

                Ray rayPlayer = new Ray(gameObject.transform.position, gameObject.transform.forward);
                destination = GetClosestPointOnFirstRay(rayPlayer, screenRay);
                goForTarget = true;
                Debug.Log("GoForTarget" + destination);



            }

            // luetaan oikean näppäimen vapautus
            if (Input.GetMouseButtonUp((int)MouseButton.Right))
            {
                Ray ray = Camera.main.ScreenPointToRay(Input.mousePosition);
                RaycastHit hitInfo;
                bool hits = Physics.Raycast(ray, out hitInfo);
                if (hits)
                {
                    Item item = hitInfo.collider.gameObject.GetComponent<Item>();
                    if (item != null)
                    {
                        if (item.isPickable)
                        {
                            Debug.Log("Picking up: " + item.description);
                            // piilotetaan objekti
                            item.gameObject.renderer.enabled = false;
                            //vaihdetaan objektin paikkaa hierarkiassa
                            item.gameObject.transform.parent = GameObject.Find("Inventory").transform;
                            item.gameObject.transform.localPosition.Set(0, 0, 0);
                            item.gameObject.collider.enabled = false;
                            item.gameObject.rigidbody.useGravity = false;
                        }
                    }

                }


            }
            // liikutuslogiikka
            Vector3 tmpPos = transform.position;
            tmpPos.y = destination.y;
            float dist = Vector3.Distance(tmpPos, destination);


            //tarkistetaan onko syytä liikkua
            if (dist > 0.1f && goForTarget)
            {

                if ((transform.position.x < destination.x && Mathf.Round(transform.rotation.eulerAngles.y) != 90)
                ||

                (transform.position.x > destination.x && Mathf.Round(transform.rotation.eulerAngles.y) != 270))
                {
                    transform.Rotate(Vector3.up * 180);
                }
                animation.CrossFade("run");
                gameObject.transform.position = Vector3.MoveTowards(gameObject.transform.position, destination, 2.5f * Time.deltaTime);



            }
            else
            {
                goForTarget = false;
            }
            //hypyn käynnistys
            if (Input.GetKeyDown(KeyCode.Space) && isJumping == false)
            {
                //hyppy ylöspäin
                animation.CrossFade("jump");
                jumpVector = transform.forward + Vector3.up;
                stayedVerticallyInPlace = 0.0f;
                isJumping = true;
            }
            //hypyn liike
            if (isJumping)
            {
                animation.CrossFade("jump");
                //edellinen sijainti talteen
                previousPosition = transform.position;

                transform.position += jumpVector * jumpForce;

                jumpVector *= 0.9f;

                previousPosition.x = transform.position.x;
                previousPosition.z = transform.position.z;

                float distance = Vector3.Distance(transform.position, previousPosition);
                if (distance < 0.001f)
                {
                    stayedVerticallyInPlace += Time.deltaTime;
                    if (stayedVerticallyInPlace > timeToStayVerticallyInPlace)
                    {
                        isJumping = false;
                    }
                }

            }
        }
        else
        {
            animation.CrossFade("idle");
        }



    }
}
