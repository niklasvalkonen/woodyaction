﻿using UnityEngine;
using System.Collections;

public class BatAttackController : MonoBehaviour
{

    public float batSpawningInterval = 0.5f;
    public static float batAttackHoldTime = 3.0f;
    public static int batsSpawned = 0;
    bool spawning = false;

    public Transform bat;
    public Transform batSwarmCenterPoint;

    
    public static bool attacking = false;
    public static bool batsWaiting = false;
    public static int batsPerWave = 3;
    public static bool waitTimeOn = false;



    // Miten taulukoilla saa aikaan hyökkäysaallot?

    void Start()
    {

    }

    IEnumerator SpawnBat()
    {
        yield return new WaitForSeconds(batSpawningInterval);
        spawning = false;
        Instantiate(bat, CreateRandomSpawnPoint(), Quaternion.Euler(0, 180.0f, 180.0f));    // Tallenna taulukkoon
        batsSpawned++;
    }


    void Update()
    {

        if (RiseScript.gameOn && !spawning && batsSpawned < batsPerWave)
        {
            spawning = true;
            StartCoroutine(SpawnBat());
        }

        if (batsSpawned >= batsPerWave && !waitTimeOn)
        {
            StartCoroutine(Wait());
        }
    }


    IEnumerator Wait()
    {
        waitTimeOn = true;
        batsWaiting = true;
        yield return new WaitForSeconds(batAttackHoldTime);
        batsWaiting = false;
        attacking = true;
    }


    Vector3 CreateRandomSpawnPoint()
    {
        float angle = Random.Range(-Mathf.PI / 3, Mathf.PI / 3);
        Vector3 spawnPoint = batSwarmCenterPoint.position + (new Vector3(Mathf.Sin(angle), Mathf.Cos(angle), 0.0f) * Random.Range(4.0f, 6.0f));
        return spawnPoint;
    }
}